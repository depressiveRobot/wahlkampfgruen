FROM node:10.15.3-alpine as builder
WORKDIR /home/node/app
COPY src src
COPY config config
COPY package.json package.json
COPY package-lock.json package-lock.json
RUN npm install --production

FROM node:10.15.3-alpine as app
WORKDIR /home/node/app
COPY --from=builder /home/node/app .
EXPOSE 3000
ENV NODE_ENV prod
CMD node src/app.js

# List of standard targets:
# https://www.gnu.org/prep/standards/html_node/Standard-Targets.html

#############################
## Front Matter            ##
#############################

.PHONY: help

.DEFAULT_GOAL := help

export NODE_ENV?=dev

#############################
## Targets                 ##
#############################

## Build application image
build:
	docker-compose build

## Perform self-tests
check:
	@if [ ! -d "./node_modules" ]; then make init; fi
	docker-compose run --rm devenv ./node_modules/.bin/eslint src/
	docker-compose run --rm devenv npm test

## Delete everything created by this repository (containers, images, files)
clean:
	docker-compose down -v --rmi all
	docker image prune -f
	rm -rf data
	rm -rf node_modules

## Stop and remove application container
down:
	docker-compose down -v

## Initialize development environment
init:
	docker-compose run --rm devenv npm install

## Install package (usage: make install ARG=<package>)
install:
	@if [ ! -d "./node_modules" ]; then make init; fi
	docker-compose run --rm devenv npm install ${ARG}

## View output of application container
logs:
	docker-compose logs -f

## Stop application container
stop:
	docker-compose stop

## Uninstall package (usage: make uninstall ARG=<package>)
uninstall:
	@if [ ! -d "./node_modules" ]; then make init; fi
	docker-compose run --rm devenv npm uninstall ${ARG}

## Start application container
up:
	docker-compose up -d app

## Start application container in development mode
up-dev:
	@if [ ! -d "./node_modules" ]; then make init; fi
	docker-compose up -d devenv

#############################
## Help Target             ##
#############################

## Show help
help:
	@printf "Available targets:\n\n"
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  %-20s %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

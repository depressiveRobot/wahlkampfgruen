# Wahlkampfgrün

[![license](https://img.shields.io/badge/license-MIT-green.svg)](https://opensource.org/licenses/MIT)
[![readme style](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)
[![make help](https://img.shields.io/badge/make-help-brightgreen.svg)](https://gitlab.com/depressiveRobot/make-help)
[![code style](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

> Present a Greens election campaign online.

## Table of Contents

* [Install](#install)
* [Usage](#usage)
* [Configuration](#configuration)
* [Development](#development)
* [Who uses it](#who-uses-it)
* [Contributing](#contributing)
* [License](#license)

## Install

Install the application by building the Docker image:

```bash
make build
```

### Dependencies

* [Make](https://www.gnu.org/software/make/)
* [Docker](https://www.docker.com/)
* [Docker Compose](https://docs.docker.com/compose/overview/)

## Usage

Run the application by starting the container and print out the logs:

```bash
make up logs
```

See `make help` for all available targets.

## Configuration

The default configuration is stored in [`./config/default.yml`](./config/default.yml) and can be overridden and extended.

To override the default configuration for a production deployment (assuming `NODE_ENV=prod`), place a custom configuration file named `prod.yml` in the `./config` folder.

For more configuration capabilities refer to the [Configuration Files](https://github.com/lorenwest/node-config/wiki/Configuration-Files) GitHub wiki page of the [node-config](https://github.com/lorenwest/node-config) package.

### Environment variables

| Variable   | Default | Description                  |
|------------|---------|------------------------------|
| `NODE_ENV` | `dev`   | The application environment. |
| `PORT`     | `3000`  | The port to bind.            |

## Development

For easier development, start in development mode which automatically restarts the application when `.js` and `.yml` file changes are detected:

```bash
make up-dev
```

The `NODE_ENV` variable can be overridden during development by specifying it via command line:

```bash
NODE_ENV=foo make up-dev
```

## Who uses it

Currently, we support the following deployments:

* [Demo](https://beispiel.weilwirhierleben.de)
* [KV Bautzen](https://bautzen.weilwirhierleben.de/)
* [KV Börde](https://boerde.weilwirhierleben.de/)
* [KV Chemnitz](https://chemnitz.weilwirhierleben.de)
* [KV Dresden](https://dresden.weilwirhierleben.de)
* [KV Görlitz](https://goerlitz.weilwirhierleben.de/)
* [KV Landkreis Leipzig](https://landkreisleipzig.weilwirhierleben.de)
* [KV Leipzig](https://leipzig.weilwirhierleben.de)
* [KV Meißen](https://meissen.weilwirhierleben.de/)
* [KV Mittelsachsen](https://mittelsachsen.weilwirhierleben.de/)
* [KV Sächsische Schweiz-Osterzgebirge](https://ssoe.weilwirhierleben.de/)
* [KV Vogtland](https://vogtland.weilwirhierleben.de/)
* [KV Zwickau](https://zwickau.weilwirhierleben.de/)

## Contributing

PRs accepted.

Git branching model based on [Gitflow](https://nvie.com/posts/a-successful-git-branching-model/).

Branch naming convention:

* Feature branches: `feature/*`
* Release branches: `release/X.Y.Z`

Tag naming convention:

* Release tags: `X.Y.Z`

Code formatting with [Prettier](https://prettier.io/) using default options.

Test your changes and ensure they all pass without issue:

```bash
make check
```

## License

[MIT](https://opensource.org/licenses/MIT) © 2019 Marvin Frommhold and Daniel Gerber

See license in [LICENSE.txt](./LICENSE.txt)

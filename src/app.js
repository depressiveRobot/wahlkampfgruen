require("strict-mode")(() => {
    const config = require("config");
    const express = require("express");
    const hbs = require("express-hbs");
    const bodyParser = require("body-parser");
    const cookieParser = require("cookie-parser");
    const expressSession = require("express-session");
    const flash = require("connect-flash");
    const passport = require("./auth");

    const app = express();

    // set Handlebars as view engine
    app.engine(
        "hbs",
        hbs.express4({
            defaultLayout: `${__dirname}/views/layouts/default.hbs`
        })
    );
    app.set("view engine", "hbs");
    app.set("views", `${__dirname}/views`);

    // set static resources folder
    app.use(express.static(`${__dirname}/public`));

    // set upload limit for application/json
    app.use(bodyParser.json({ limit: "5mb" }));

    app.use(bodyParser.urlencoded({ extended: false }));

    // enable session handling
    app.use(cookieParser());
    app.use(
        expressSession({
            secret: `${config.app.auth.sessionSecret}`,
            resave: false,
            saveUninitialized: false
        })
    );

    // set passport for authentication
    app.use(passport.initialize());
    // restore authentication state, if any, from the session
    app.use(passport.session());
    app.use(flash());

    // mount routes
    app.use(require("./routes"));

    // mount server
    app.listen(config.get("app.port"), () => {
        console.info(
            `Wahlkampfgruen (${config.app.env}) running on http://localhost:${config.app.port}`
        );
    });
});

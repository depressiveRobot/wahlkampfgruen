const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const _ = require("lodash");
const config = require("config");

// helper function
function findUserByUsername(username) {
    return _.find(config.site.users, { username: username });
}

// set local strategy used by passport
//
// Verifies that the password is correct and invokes `callback` with a user object,
// which will be set at `req.user` in route handlers after authentication.
passport.use(
    new LocalStrategy({ passReqToCallback: true }, (req, username, password, callback) => {
        const user = findUserByUsername(username);
        if (user) {
            if (user.password == password) {
                // successful login
                return callback(null, user);
            }
        }
        // wrong credentials
        req.flash("message", "Ungültiger Name oder Passwort.");
        return callback(null, false);
    })
);

// authenticated session persistence.
passport.serializeUser((user, callback) => {
    callback(null, user.username);
});
passport.deserializeUser((username, callback) => {
    const user = findUserByUsername(username);
    if (user) {
        callback(null, user);
    } else {
        callback(new Error(`Unknown username: '${username}'`));
    }
});

module.exports = passport;

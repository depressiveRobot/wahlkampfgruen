const candidacy = [
    {
        key: "headline",
        label: "Schlagzeile",
        description:
            "Wird in der Kandidat*innenübersicht unter deinem Profilbild und in der Profilansicht angezeigt.",
        type: "textarea",
        showCharCount: true,
        inputFormat: "plain",
        validate: {
            required: true,
            customMessage: "Erforderlich, maximal 140 Zeichen.",
            maxLength: 140
        }
    },
    {
        key: "speech",
        label: "Bewerbungsrede",
        type: "textarea",
        showCharCount: true,
        inputFormat: "plain",
        rows: 12,
        validate: {
            required: true,
            customMessage: "Erforderlich, maximal 2000 Zeichen.",
            maxLength: 2000
        }
    },
    {
        key: "youtubeLink",
        label: "YouTube",
        description:
            "Link zu deinem Bewerbungsvideo oder ein anderes Video, welches deine Botschaft vermittelt.",
        type: "url",
        inputFormat: "plain",
        validate: {
            custom:
                "if (input) valid = new RegExp('^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?(youtube\\.com|youtu\\.be)\\/[A-z 0-9 _ - \\.]+\\/?').test(input);",
            customMessage:
                "Ungültiger YouTube-Link. (Beispiel: https://www.youtube.com/watch?v=RcSOmbDnUbw)"
        }
    },
    {
        key: "youtubeCaption",
        label: "Beschreibung zum Video",
        type: "textarea",
        showCharCount: true,
        inputFormat: "plain",
        validate: {
            customMessage: "Maximal 300 Zeichen.",
            maxLength: 300
        }
    },
    {
        key: "activities",
        label: "Tätigkeiten",
        type: "datagrid",
        addAnother: "Tätigkeit",
        validate: {
            minLength: 0,
            maxLength: 10
        },
        components: [
            {
                key: "from",
                label: "Von",
                type: "number",
                requireDecimal: false,
                validate: {
                    integer: true,
                    customMessage: "Eine Jahreszahl zwischen 1900 und dem aktuellem Jahr.",
                    custom: "if (input) valid = input <= new Date().getFullYear() && input >= 1900;"
                }
            },
            {
                key: "to",
                label: "Bis",
                type: "number",
                requireDecimal: false,
                validate: {
                    integer: true,
                    customMessage: "Eine Jahreszahl zwischen 1900 und dem aktuellem Jahr.",
                    custom: "if (input) valid = input <= new Date().getFullYear() && input >= 1900;"
                }
            },
            {
                key: "description",
                label: "Beschreibung",
                type: "textfield",
                inputFormat: "plain",
                validate: {
                    customMessage: "Maximal 140 Zeichen.",
                    maxLength: 140
                }
            },
            {
                key: "link",
                label: "Link",
                type: "url",
                inputFormat: "plain",
                validate: {
                    custom:
                        "if (input) valid = new RegExp('^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)[A-z 0-9 _ - \\.]').test(input);",
                    customMessage: "Ungültiger Link. (Beispiel: https://gruene.de)"
                }
            }
        ]
    },
    {
        key: "memberships",
        label: "Mitgliedschaften",
        type: "datagrid",
        addAnother: "Mitgliedschaft",
        validate: {
            minLength: 0,
            maxLength: 10
        },
        components: [
            {
                key: "from",
                label: "Von",
                type: "number",
                requireDecimal: false,
                validate: {
                    integer: true,
                    customMessage: "Eine Jahreszahl zwischen 1900 und dem aktuellem Jahr.",
                    custom: "if (input) valid = input <= new Date().getFullYear() && input >= 1900;"
                }
            },
            {
                key: "to",
                label: "Bis",
                type: "number",
                requireDecimal: false,
                validate: {
                    integer: true,
                    customMessage: "Eine Jahreszahl zwischen 1900 und dem aktuellem Jahr.",
                    custom: "if (input) valid = input <= new Date().getFullYear() && input >= 1900;"
                }
            },
            {
                key: "description",
                label: "Beschreibung",
                type: "textfield",
                inputFormat: "plain",
                validate: {
                    customMessage: "Maximal 140 Zeichen.",
                    maxLength: 140
                }
            },
            {
                key: "link",
                label: "Link",
                type: "url",
                inputFormat: "plain",
                validate: {
                    custom:
                        "if (input) valid = new RegExp('^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)[A-z 0-9 _ - \\.]').test(input);",
                    customMessage: "Ungültiger Link. (Beispiel: https://gruene.de)"
                }
            }
        ]
    }
];

module.exports = candidacy;

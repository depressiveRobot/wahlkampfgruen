const contact = [
    {
        key: "website",
        label: "Webseite",
        description: "Link zu deiner Webseite.",
        type: "url",
        inputFormat: "plain",
        validate: {
            custom:
                "if (input) valid = new RegExp('^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)[A-z 0-9 _ - \\.]').test(input);",
            customMessage: "Ungültiger Link. (Beispiel: https://gruene.de)"
        }
    },
    {
        key: "facebook",
        label: "Facebook",
        description: "Link zu deinem Facebook-Profil.",
        type: "url",
        inputFormat: "plain",
        validate: {
            custom:
                "if (input) valid = new RegExp('^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)(facebook|fb)\\.com\\/[A-z 0-9 _ - \\.]+\\/?').test(input);",
            customMessage:
                "Ungültiger Facebook-Link. (Beispiel: https://www.facebook.com/B90DieGruenen)"
        }
    },
    {
        key: "twitter",
        label: "Twitter",
        description: "Link zu deinem Twitter-Profil.",
        type: "url",
        inputFormat: "plain",
        validate: {
            custom:
                "if (input) valid = new RegExp('^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)twitter\\.com\\/[A-z 0-9 _]+\\/?').test(input);",
            customMessage: "Ungültiger Twitter-Link. (Beispiel: https://twitter.com/die_gruenen)"
        }
    },
    {
        key: "instagram",
        label: "Instagram",
        description: "Link zu deinem Instagram-Profil.",
        type: "url",
        inputFormat: "plain",
        validate: {
            custom:
                "if (input) valid = new RegExp('^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)instagram\\.com\\/([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\\.(?!\\.))){0,28}(?:[A-Za-z0-9_]))?)').test(input);",
            customMessage:
                "Ungültiger Instagram-Link. (Beispiel: https://instagram.com/die_gruenen)"
        }
    },
    {
        key: "email",
        label: "Email",
        description: "Deine Email-Adresse für den Wahlkampf.",
        type: "email",
        validate: {
            custom:
                "if (input) valid = new RegExp('^[a-zA-Z0-9.!#$%&\\'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$').test(input);",
            customMessage: "Ungültige Email-Adresse. (Beispiel: info@gruene.de)"
        }
    },
    {
        key: "telephone",
        label: "Telefonnummer",
        description: "Deine Telefonnummer für den Wahlkampf.",
        type: "textfield",
        inputFormat: "plain",
        validate: {
            custom: "if (input) valid = new RegExp('^[0-9]+$').test(input);",
            customMessage: "Nur Zahlen, keine andere Zeichen."
        }
    }
];

module.exports = contact;

const person = require("./person");
const contact = require("./contact");
const candidacy = require("./candidacy");

const formio = {
    display: "form",
    components: [
        {
            key: "person",
            legend: "Persönliches",
            type: "fieldset",
            components: person
        },
        {
            key: "social",
            legend: "Kontakt",
            type: "fieldset",
            components: contact
        },
        {
            key: "candidacy",
            legend: "Bewerbung",
            type: "fieldset",
            components: candidacy
        },
        {
            key: "submit",
            label: "Speichern",
            type: "button",
            theme: "primary",
            size: "",
            disableOnInvalid: true
        }
    ]
};

module.exports = formio;

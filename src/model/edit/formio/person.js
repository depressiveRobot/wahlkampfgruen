const person = [
    {
        key: "title",
        label: "Titel",
        type: "textfield",
        inputFormat: "plain",
        validate: {
            customMessage: "Maximal 20 Zeichen.",
            maxLength: 20
        }
    },
    {
        key: "forename",
        label: "Vorname",
        type: "textfield",
        inputFormat: "plain",
        validate: {
            required: true,
            customMessage: "Erforderlich, maximal 50 Zeichen.",
            maxLength: 50
        }
    },
    {
        key: "surname",
        label: "Nachname",
        type: "textfield",
        inputFormat: "plain",
        validate: {
            required: true,
            customMessage: "Erforderlich, maximal 50 Zeichen.",
            maxLength: 50
        }
    },
    {
        key: "age",
        label: "Alter",
        type: "number",
        requireDecimal: false,
        validate: {
            integer: true,
            customMessage: "Eine Zahl zwischen 18 und 100.",
            min: 18,
            max: 100
        }
    },
    {
        key: "personalStatus",
        label: "Familienstand",
        type: "textfield",
        inputFormat: "plain",
        validate: {
            customMessage: "Maximal 50 Zeichen.",
            maxLength: 50
        }
    },
    {
        key: "children",
        label: "Kinder",
        type: "select",
        data: {
            values: [
                {
                    label: "keine",
                    value: 0
                },
                {
                    label: "1",
                    value: 1
                },
                {
                    label: "2",
                    value: 2
                },
                {
                    label: "3",
                    value: 3
                },
                {
                    label: "4",
                    value: 4
                },
                {
                    label: "5",
                    value: 5
                },
                {
                    label: "6",
                    value: 6
                },
                {
                    label: "7",
                    value: 7
                },
                {
                    label: "8",
                    value: 8
                },
                {
                    label: "9",
                    value: 9
                }
            ]
        }
    },
    {
        key: "qualification",
        label: "Ausbildung",
        type: "textfield",
        inputFormat: "plain",
        validate: {
            customMessage: "Maximal 50 Zeichen.",
            maxLength: 50
        }
    },
    {
        key: "job",
        label: "Beruf",
        type: "textfield",
        inputFormat: "plain",
        validate: {
            customMessage: "Maximal 50 Zeichen.",
            maxLength: 50
        }
    },
    {
        key: "confession",
        label: "Konfession",
        type: "textfield",
        inputFormat: "plain",
        validate: {
            customMessage: "Maximal 50 Zeichen.",
            maxLength: 50
        }
    },
    {
        key: "memberSince",
        label: "Eintrittsjahr Grüne",
        type: "number",
        requireDecimal: false,
        validate: {
            integer: true,
            customMessage: "Eine Jahreszahl zwischen 1980 und dem aktuellem Jahr.",
            custom: "if (input) valid = input <= new Date().getFullYear() && input >= 1980;"
        }
    },
    {
        key: "favoritePlace",
        label: "Lieblingsort",
        type: "textfield",
        inputFormat: "plain",
        validate: {
            customMessage: "Maximal 50 Zeichen.",
            maxLength: 50
        }
    },
    {
        key: "funFact",
        label: "Verrücktes",
        description: "Ein interessanter Fakt über dich.",
        type: "textfield",
        inputFormat: "plain",
        validate: {
            customMessage: "Maximal 140 Zeichen.",
            maxLength: 140
        }
    }
];

module.exports = person;

const _ = require("lodash");
const fs = require("fs-extra");
const sprintf = require("sprintf-js").sprintf;
const util = require("./util");
const config = require("config");

const profilesPath = __dirname + "/../../data/%s/profiles";
const profileFileName = profilesPath + "/%s.json";
const profileImagesPath = __dirname + "/../../data/%s/images";
const profileImageFileName = profileImagesPath + "/%s.jpeg";

const election = {
    /**
     * Finds an election.
     *
     * @param {number} id - the ID of the election
     *
     * @returns the election object or `undefined`
     */
    findById: function(id) {
        return _.find(config.site.elections, election => {
            return election.id == id;
        });
    },

    /**
     * Finds an election by its slugged name.
     *
     * @param {string} sluggedName - the slugged name of the election
     *
     * @returns the election object or `undefined`
     */
    findBySluggedName: function(sluggedName) {
        return _.find(config.site.elections, election => {
            return util.slug(election.name) == sluggedName;
        });
    }
};

const constituency = {
    /**
     * Finds a constituency of an election.
     *
     * @param {number} id - the ID of the constituency
     * @param {number} electionId - the ID of the election
     *
     * @returns the constituency object or `undefined`
     */
    findByIdAndElection: function(id, electionId) {
        return _.find(election.findById(electionId).constituencies, constituency => {
            return constituency.id == id;
        });
    }
};

const profile = {
    /**
     * Finds all profiles of an election.
     *
     * The returned array of profiles will be sorted first by constituency and second by position.
     *
     * @param {number} id - the ID of the election
     *
     * @returns a `Promise` which resolves with a sorted array of profiles as JSON objects
     */
    findAllByElection: function(id) {
        return new Promise((resolve, reject) => {
            const dir = sprintf(profilesPath, id);
            fs.readdir(dir, { withFileTypes: true })
                .then(files => {
                    const profiles = [];
                    files.forEach(file => {
                        if (file.name.endsWith(".json")) {
                            // FIXME catch exception during read
                            profiles.push(fs.readJsonSync(dir + "/" + file.name, "utf8").data);
                        }
                    });
                    resolve(util.sortProfiles(profiles));
                })
                .catch(error => reject(error));
        });
    },

    /**
     * Finds a profile of an election.
     *
     * @param {number} id - the ID of the profile
     * @param {number} electionId - the ID of the election
     *
     * @returns a `Promise` which resolves with the profile as JSON object or `null` if no such
     *          profile exists
     */
    findByIdAndElection: function(id, electionId) {
        return new Promise((resolve, reject) => {
            const fileName = sprintf(profileFileName, electionId, id);
            fs.access(fileName, fs.constants.R_OK)
                .then(() => {
                    fs.readJson(fileName)
                        .then(profile => resolve(profile.data))
                        .catch(error => reject(error));
                })
                .catch(() => resolve(null));
        });
    },

    /**
     * Saves a profile of an election.
     *
     * @param {number} id - the ID of the profile
     * @param {number} electionId - the ID of the election
     * @param {object} data - the profile data
     *
     * @returns a `Promise` which resolves if the data was saved successfully
     */
    saveByIdAndElection: function(id, electionId, data) {
        return new Promise((resolve, reject) => {
            const fileName = sprintf(profileFileName, electionId, id);
            fs.outputJson(fileName, data, { spaces: 4 })
                .then(() => resolve())
                .catch(error => reject(error));
        });
    }
};

const image = {
    /**
     * Finds a profile image of an election.
     *
     * @param {number} id - the ID of the profile
     * @param {number} electionId - the ID of the election
     *
     * @returns a `Promise` which resolves with a `Buffer` of the image file or `null` if no such
     *          profile image exists
     */
    findByIdAndElection: function(id, electionId) {
        return new Promise((resolve, reject) => {
            const fileName = sprintf(profileImageFileName, electionId, id);
            fs.access(fileName, fs.constants.R_OK)
                .then(() => {
                    fs.readFile(fileName)
                        .then(buffer => resolve(buffer))
                        .catch(error => reject(error));
                })
                .catch(() => resolve(null));
        });
    },

    /**
     * Saves a profile image of an election.
     *
     * @param {number} id - the ID of the profile
     * @param {number} electionId - the ID of the election
     * @param {string} image - the base64 image data
     *
     * @returns a `Promise` which resolves if the image was saved successfully
     */
    saveByIdAndElection: function(id, electionId, image) {
        return new Promise((resolve, reject) => {
            const fileName = sprintf(profileImageFileName, electionId, id);
            fs.outputFile(fileName, image, { encoding: "base64" })
                .then(() => resolve())
                .catch(error => reject(error));
        });
    }
};

module.exports = { election, constituency, profile, image };

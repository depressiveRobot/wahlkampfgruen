const getYouTubeID = require("get-youtube-id");
const _ = require("lodash");
const slugify = require("slugify");
const config = require("config");

// support umlauts
slugify.extend({ Ä: "AE", Ö: "OE", Ü: "UE", ä: "ae", ö: "oe", ü: "ue" });

/**
 * Slugifies the given string.
 *
 * @param {string} str
 *     the string to slug
 *
 * @returns the slugged lower case string
 *
 * @see https://github.com/simov/slugify
 */
function slug(str) {
    return slugify(str, {
        lower: true
    });
}

/**
 * Rewrites the given YouTube link so that it can be
 * used for embedding the video in a website.
 *
 * @param {string} youtubeLink
 *     the YouTube link to rewrite
 *
 * @returns the link to embed
 */
function toYoutubeEmbeddedLink(youtubeLink) {
    const id = getYouTubeID(youtubeLink);
    return id ? `https://www.youtube-nocookie.com/embed/${id}` : null;
}

/**
 * Finds the name of the electorate for the given ID.
 *
 * @param {string} id
 *   the ID to find the electorate name for
 *
 * @returns the name of the electorate
 */
// TODO: remove?
function findElectorateNameById(id) {
    return _.find(config.site.electorates, { id: id }).name;
}

/**
 * Finds the name of the electorate for the given username.
 *
 * @param {string} username
 *   the username to find the electorate for
 *
 * @returns the name of the electorate
 */
// TODO: remove?
function findElectorateNameByUsername(username) {
    return findElectorateNameById(_.find(config.site.users, { username: username }).electorate);
}

/**
 * Sorts the given array of profile data
 * first by `constituency` and second by `position`.
 *
 * @param {object} profiles
 *   the profile data to be sorted
 *
 * @returns the sorted profile data
 */
function sortProfiles(profiles) {
    return _.orderBy(profiles, [
        item => {
            return Number(item.constituency);
        },
        item => {
            return Number(item.position);
        }
    ]);
}

/**
 * Sets the prefix "Wahlkreis " to the given electorate name
 * if it doesn't start with "Wahlkreis ".
 *
 * @param {string} electorateName
 *   the name of the electorate
 *
 * @returns the electorate name with prefix set if needed
 */
function setElectorateNamePrefix(electorateName) {
    if (electorateName.startsWith("Wahlkreis ")) {
        return electorateName;
    } else {
        return "Wahlkreis " + electorateName;
    }
}

module.exports = {
    slug,
    toYoutubeEmbeddedLink,
    findElectorateNameById,
    findElectorateNameByUsername,
    sortProfiles,
    setElectorateNamePrefix
};

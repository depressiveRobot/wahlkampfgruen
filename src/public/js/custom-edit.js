// formData, profileData and sluggedElectionName vars must be initialized in `edit.hbs`
var isProfileImageSet = false;
var cropper;
var image = $("#profileImageCropElement");

// initialize profile edit form
Formio.icons = "fontawesome";
Formio.createForm(document.getElementById("formio"), formData, { noAlerts: true }).then(form => {
    if (profileData) {
        form.submission = { "data": profileData };
    }

    // register for submit event to save profile data
    form.on("submit", submission => {
        if (isProfileImageSet) {
            submission.data.profileImage = cropper
                .getCroppedCanvas({
                    width: 600,
                    height: 600
                })
                .toDataURL("image/jpeg", 0.8);
            submitProfile(submission);
        } else {
            if (submission.data.profileImage) {
                submitProfile(submission);
            } else {
                $("#profileImage").addClass("has-error alert alert-danger");
                $("#profileImage")
                    .find(".formio-errors")
                    .show();
                $(".fa.fa-refresh.fa-spin.button-icon-right").remove();
                $("#upload-incomplete").show();
                $("#upload-success").hide();
                $("#upload-error").hide();
            }
        }
    });

    form.on("error", errors => {
        if (!isProfileImageSet) {
            $("#profileImage").addClass("has-error alert alert-danger");
            $("#profileImage")
                .find(".formio-errors")
                .show();
        }
        $(".fa.fa-refresh.fa-spin.button-icon-right").remove();
        $("#upload-incomplete").show();
        $("#upload-success").hide();
        $("#upload-error").hide();
    });
});

// update/set profile image
$("#profileImageInput").change(event => {
    if (event.target.files && event.target.files[0]) {
        $("#profileImageCrop").show();
        var reader = new FileReader();

        reader.onload = () => {
            if (cropper) {
                cropper.replace(reader.result);
            } else {
                initializeCropper(reader.result);
            }
        };

        reader.readAsDataURL(event.target.files[0]);
        isProfileImageSet = true;
    }
});

// profile image button actions
$("#zoomIn").on("click", () => {
    cropper.zoom(0.1);
});
$("#zoomOut").on("click", () => {
    cropper.zoom(-0.1);
});
$("#moveLeft").on("click", () => {
    cropper.move(-10, 0);
});
$("#moveRight").on("click", () => {
    cropper.move(10, 0);
});
$("#moveUp").on("click", () => {
    cropper.move(0, -10);
});
$("#moveDown").on("click", () => {
    cropper.move(0, 10);
});
$("#rotateRight").on("click", () => {
    cropper.rotate(90);
});

function initializeCropper(imageUrl) {
    image.cropper({
        viewMode: 3,
        dragMode: "move",
        aspectRatio: 1,
        autoCropArea: 1,
        restore: false,
        guides: false,
        center: false,
        highlight: false,
        cropBoxMovable: false,
        cropBoxResizable: false,
        toggleDragModeOnDblclick: false,
        minContainerWidth: 300,
        minContainerHeight: 300
    });
    cropper = image.data("cropper");
    cropper.replace(imageUrl);
}

function submitProfile(profileData) {
    $.ajax({
        type: "POST",
        url: "/bearbeiten/" + sluggedElectionName,
        data: JSON.stringify(profileData),
        contentType: "application/json"
    }).then(
        () => {
            // submit success
            $(".fa.fa-refresh.fa-spin.button-icon-right").remove();
            $("#upload-success").show();
            $("#upload-error").hide();
            $("#upload-incomplete").hide();
            $("#profileImage").removeClass("has-error alert alert-danger");
            $("#profileImage")
                .find(".formio-errors")
                .hide();
        },
        () => {
            // submit error
            $(".fa.fa-refresh.fa-spin.button-icon-right").remove();
            $("#upload-error").show();
            $("#upload-success").hide();
            $("#upload-incomplete").hide();
        }
    );
}

$(document).ready(() => {
    bsCustomFileInput.init();
    $("#profileImage").insertAfter($(".formio-component-surname"));

    // profile image was previously saved
    if (profileData && profileData.profileImage) {
        isProfileImageSet = true;
        $("#profileImageCrop").show();
        initializeCropper(profileData.profileImage);
    }
});

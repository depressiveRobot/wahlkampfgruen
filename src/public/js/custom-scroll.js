// animate scroll to link target
$(".scroll-link").click(event => {
    event.preventDefault();
    var aid = $(event.target).attr("href");
    $("html,body").animate({
        scrollTop: $(aid).offset().top
    });
});

// show scrollTop button when scrolled down 100px from top
$(window).scroll(event => {
    if ($(document.body).scrollTop() > 100 || $(document.documentElement).scrollTop() > 100) {
        $("#scrollTopBtn").show();
    } else {
        $("#scrollTopBtn").hide();
    }
});

// scroll to the top of the document
$("#scrollTopBtn").click(event => {
    $("html,body").animate({
        scrollTop: 0
    });
});

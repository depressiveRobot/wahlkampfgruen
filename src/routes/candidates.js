const router = require("express").Router();
const config = require("config");
const util = require("../model/util");
const repository = require("../model/repository");

/**
 * Redirects to the default election.
 */
router.get("/", (req, res) => {
    res.redirect(`/kandidat-innen/${util.slug(config.site.elections[0].name)}`);
});

/**
 * Renders the election candidates page.
 */
router.get("/:election", (req, res, next) => {
    const election = repository.election.findBySluggedName(req.params.election);
    const elections = config.site.elections.length > 1 ? config.site.elections : null;
    if (election) {
        repository.profile
            .findAllByElection(election.id)
            .then(profiles => {
                res.render("candidates", {
                    config: config,
                    username: req.user ? req.user.username : null,
                    profiles: profiles,
                    elections: elections,
                    selectedElection: election
                });
            })
            .catch(error => {
                console.error("Error loading profiles: " + error.message);
                res.render("candidates", {
                    config: config,
                    username: req.user ? req.user.username : null,
                    profiles: [],
                    elections: elections,
                    selectedElection: election
                });
            });
    } else {
        // unknown election
        next();
    }
});

module.exports = router;

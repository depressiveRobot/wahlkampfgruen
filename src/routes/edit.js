const router = require("express").Router();
const connectEnsureLogin = require("connect-ensure-login");
const _ = require("lodash");
const formio = require("../model/edit/formio/formio");
const dataUrls = require("data-urls");
const config = require("config");
const repository = require("../model/repository");

function internalServerError(res, err) {
    console.error(err);
    res
        // Problem Details for HTTP APIs (https://tools.ietf.org/html/rfc7807)
        .type("application/problem+json")
        .status(500)
        .send({
            title: "Something went wrong.",
            detail: err.message,
            status: 500
        });
}

function badRequest(message, res) {
    res
        // Problem Details for HTTP APIs (https://tools.ietf.org/html/rfc7807)
        .type("application/problem+json")
        .status(400)
        .send({
            title: message,
            status: 400
        });
}

/**
 * Renders the profile selection page.
 */
router.get("/", connectEnsureLogin.ensureLoggedIn("/anmelden"), (req, res) => {
    const candidacies = [];
    req.user.candidacies.forEach(candidacy => {
        candidacies.push({
            electionName: repository.election.findById(candidacy.election).name,
            constituencyName: repository.constituency.findByIdAndElection(
                candidacy.constituency,
                candidacy.election
            ).name,
            position: candidacy.position
        });
    });
    res.render("editSelection", {
        config: config,
        username: req.user.username,
        candidacies: candidacies
    });
});

/**
 * Renders the profile edit page.
 */
router.get("/:election", connectEnsureLogin.ensureLoggedIn("/anmelden"), (req, res, next) => {
    const election = repository.election.findBySluggedName(req.params.election);
    if (election) {
        const candidacy = _.find(req.user.candidacies, candidacy => {
            return candidacy.election == election.id;
        });
        if (candidacy) {
            const constituency = repository.constituency.findByIdAndElection(
                candidacy.constituency,
                election.id
            );
            repository.profile
                .findByIdAndElection(req.user.username, election.id)
                .then(profile => {
                    if (profile) {
                        // edit existing profile data
                        res.render("editProfile", {
                            config: config,
                            username: req.user.username,
                            form: JSON.stringify(formio),
                            candidacy: {
                                electionName: election.name,
                                constituencyName: constituency.name,
                                position: candidacy.position
                            },
                            profileData: JSON.stringify(profile)
                        });
                    } else {
                        // edit profile data from scratch
                        res.render("editProfile", {
                            config: config,
                            username: req.user.username,
                            form: JSON.stringify(formio),
                            candidacy: {
                                electionName: election.name,
                                constituencyName: constituency.name,
                                position: candidacy.position
                            }
                        });
                    }
                })
                .catch(error => {
                    console.warn("Cannot read profile: " + error.message);
                    // FIXME should this be an internal server error?
                    next();
                });
        } else {
            // the user is not a candidate of this election
            // FIXME should this be a bad request?
            next();
        }
    } else {
        // unknown election
        // FIXME should this be a bad request?
        next();
    }
});

/**
 * Saves the profile data.
 */
router.post("/:election", connectEnsureLogin.ensureLoggedIn("/anmelden"), (req, res) => {
    const election = repository.election.findBySluggedName(req.params.election);
    if (election) {
        const candidacy = _.find(req.user.candidacies, candidacy => {
            return candidacy.election == election.id;
        });
        if (candidacy) {
            if (req.body) {
                // additional information
                req.body.data.id = req.user.username;
                req.body.data.election = candidacy.election;
                req.body.data.constituency = candidacy.constituency;
                req.body.data.position = candidacy.position;

                repository.profile
                    .saveByIdAndElection(req.user.username, candidacy.election, req.body)
                    .then(() => {
                        repository.image
                            .saveByIdAndElection(
                                req.user.username,
                                candidacy.election,
                                dataUrls(req.body.data.profileImage).body
                            )
                            .then(() => {
                                res.end();
                            })
                            .catch(error => internalServerError(res, error));
                    })
                    .catch(error => internalServerError(res, error));
            } else {
                // no profile data found
                badRequest("Body must contain profile data.", res);
            }
        } else {
            // the user is not a candidate of this election
            badRequest("No candidacy found for this election.", res);
        }
    } else {
        // unknown election
        badRequest("Unknown election.", res);
    }
});

module.exports = router;

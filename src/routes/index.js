const router = require("express").Router();
const hbs = require("express-hbs");
const _ = require("lodash");
const util = require("../model/util");

const config = require("config");

hbs.registerHelper("debug", object => {
    console.debug(JSON.stringify(object, null, 2));
});
hbs.registerHelper("slug", str => {
    return util.slug(str);
});
hbs.registerHelper("equals", (arg1, arg2, options) => {
    return arg1 == arg2 ? options.fn(this) : options.inverse(this);
});
hbs.registerHelper("contains", (collection, key, value, options) => {
    return _.some(collection, [key, value]) ? options.fn(this) : options.inverse(this);
});
hbs.registerHelper("electorateNamePrefix", electorateName => {
    return util.setElectorateNamePrefix(electorateName);
});
hbs.registerHelper("youtubeEmbedded", text => {
    return new hbs.SafeString(util.toYoutubeEmbeddedLink(text.toString()));
});
hbs.registerHelper("headerImage", () => {
    return _.sample(config.site.localization.headerImages);
});

router.use("/", require("./login"));
router.use("/bearbeiten", require("./edit"));
router.use("/kandidat-innen", require("./candidates"));
router.use("/kandidat-innen", require("./profile"));
router.use("/wahlprogramm", require("./manifesto"));

/**
 * Displays the start page.
 */
router.get("/", (req, res) => {
    res.render("index", {
        config: config,
        username: req.user ? req.user.username : null
    });
});

/**
 * Catches all unknown requested resources.
 */
router.all("*", (req, res) => {
    res
        // Problem Details for HTTP APIs (https://tools.ietf.org/html/rfc7807)
        .contentType("application/problem+json")
        .status(404)
        .send({
            title: "Not Found",
            status: 404
        });
});

module.exports = router;

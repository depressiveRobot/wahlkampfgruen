const router = require("express").Router();
const passport = require("passport");
const config = require("config");

/**
 * Displays the login page.
 */
router.get("/anmelden", (req, res) => {
    res.render("login", {
        config: config,
        username: req.user ? req.user.username : null,
        message: req.flash("message")
    });
});

/**
 * Checks the login credentials and
 * redirects to edit page when successful.
 */
router.post(
    "/anmelden",
    passport.authenticate("local", {
        successReturnToOrRedirect: "/bearbeiten",
        failureRedirect: "/anmelden"
    })
);

/**
 * Terminates the login session and
 * redirects to the login page.
 */
router.get("/abmelden", (req, res) => {
    req.logout();
    res.redirect("/anmelden");
});

module.exports = router;

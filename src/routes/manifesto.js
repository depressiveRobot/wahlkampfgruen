const router = require("express").Router();
const config = require("config");

/**
 * Displays the manifesto.
 */
router.get("/", (req, res) => {
    res.render("manifesto", {
        config: config,
        username: req.user ? req.user.username : null,
        elections: config.site.elections
    });
});

module.exports = router;

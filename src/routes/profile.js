const router = require("express").Router();
const config = require("config");
const _ = require("lodash");
const util = require("../model/util");
const repository = require("../model/repository");

/**
 * Redirects to the default election.
 */
router.get("/:constituency/:profile", (req, res) => {
    const sluggedElectionName = util.slug(config.site.elections[0].name);
    const sluggedConstituencyName = req.params.constituency;
    const profileId = req.params.profile;

    res.redirect(`/kandidat-innen/${sluggedElectionName}/${sluggedConstituencyName}/${profileId}`);
});

/**
 * Renders the requested profile page.
 */
router.get("/:election/:constituency/:profile", (req, res, next) => {
    const election = repository.election.findBySluggedName(req.params.election);
    if (election) {
        const constituency = _.find(election.constituencies, constituency => {
            return util.slug(constituency.name) == req.params.constituency;
        });
        if (constituency) {
            repository.profile
                .findByIdAndElection(req.params.profile, election.id)
                .then(profile => {
                    if (profile) {
                        res.render("profile", {
                            config: config,
                            username: req.user ? req.user.username : null,
                            profile: profile,
                            electionName: election.name,
                            constituencyName: constituency.name,
                            supportersHtml: constituency.supportersHtml
                        });
                    } else {
                        // unknown profile
                        next();
                    }
                })
                .catch(error => {
                    console.warn("Cannot read profile: " + error.message);
                    // FIXME should we throw an internal server error?
                    next();
                });
        } else {
            // unknown constituency
            next();
        }
    } else {
        // unknown election
        next();
    }
});

/**
 * Returns the requested profile image.
 */
router.get("/:election/:constituency/:profile.jpeg", (req, res, next) => {
    const election = repository.election.findBySluggedName(req.params.election);
    if (election) {
        const constituency = _.find(election.constituencies, constituency => {
            return util.slug(constituency.name) == req.params.constituency;
        });
        if (constituency) {
            repository.image
                .findByIdAndElection(req.params.profile, election.id)
                .then(buffer => {
                    if (buffer) {
                        res.type("jpeg");
                        res.send(buffer);
                    } else {
                        // unknown profile image
                        next();
                    }
                })
                .catch(error => {
                    console.warn("Cannot read profile image: " + error.message);
                    // FIXME should we throw an internal server error?
                    next();
                });
        } else {
            // unknown constituency
            next();
        }
    } else {
        // unknown election
        next();
    }
});

module.exports = router;

const assert = require("assert");
const _ = require("lodash");
const util = require("../../src/model/util");

describe("model/util", () => {
    describe("#sortProfiles()", () => {
        it("should sort profiles first by `constituency` and second by `position`", () => {
            // create some ordered dummy data
            const expected = [];
            _.range(0, 11).forEach(i => {
                _.range(0, 11).forEach(j =>
                    expected.push({
                        constituency: `${i}`,
                        position: `${j}`
                    })
                );
            });

            // shuffle dummy data before we try to sort them
            const actual = util.sortProfiles(_.shuffle(expected));

            assert.deepEqual(expected, actual);
        });
    });

    describe("#toYoutubeEmbeddedLink()", () => {
        it("should rewrite to correct YouTube embed link", () => {
            const expected = "https://www.youtube-nocookie.com/embed/12345678901";

            const actual = util.toYoutubeEmbeddedLink("https://youtu.be/12345678901&t=10");

            assert.equal(expected, actual);
        });
        it("should return `null` if not a Youtube link", () => {
            const expected = null;

            const actual = util.toYoutubeEmbeddedLink("http://example.org/");

            assert.equal(expected, actual);
        });
    });

    describe("#slug()", () => {
        it("should return a lower case string", () => {
            const expected = "foo-bar";

            const actual = util.slug("FOO BAR");

            assert.equal(expected, actual);
        });
        it("should replace upper and lower case german umlauts", () => {
            const expected = "aeoeueaeoeue";

            const actual = util.slug("ÄÖÜäöü");

            assert.equal(expected, actual);
        });
    });

    describe("#setElectorateNamePrefix()", () => {
        it("should set prefix 'Wahlkreis '", () => {
            const expected = "Wahlkreis Foo";

            const actual = util.setElectorateNamePrefix("Foo");

            assert.equal(expected, actual);
        });
        it("shouldn't set prefix if already starts with 'Wahlkreis '", () => {
            const expected = "Wahlkreis Foo";

            const actual = util.setElectorateNamePrefix("Wahlkreis Foo");

            assert.equal(expected, actual);
        });
    });
});
